## [0.3.2](https://gitlab.com/resource-arbiter/i18n-ts/compare/v0.3.1...v0.3.2) (2019-08-06)


### Bug Fixes

* **ci:** Automates build and CHANGELOG in CI ([5584115](https://gitlab.com/resource-arbiter/i18n-ts/commit/5584115))
