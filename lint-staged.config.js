module.exports = {
  'src/**/*.{js,json,md,ts,tsx}': ['prettier --write', 'git add'],
  'src/**/*.{ts,tsx}': ['eslint']
}
