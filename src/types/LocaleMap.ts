import { MessageMap } from './MessageMap'

/**
 * Object map of message maps where keys are strings (such as locale short
 * names) and values are maps of translation message interpolators.
 *
 * @typeparam `TLocaleNames` Type of locale map keys, provided for string
 *   narrowing
 * @typeparam `TMessages` Type of message maps for each locale, provided to
 *   narrow message maps to one with specific sets of keys and messages
 */
export type LocaleMap<
  TLocaleNames extends string = string,
  TMessages extends MessageMap = MessageMap
> = Record<TLocaleNames, TMessages>
