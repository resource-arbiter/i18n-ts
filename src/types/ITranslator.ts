import { LocaleMap } from './LocaleMap'

export interface ITranslator<TLocales extends LocaleMap = LocaleMap> {
  translate<TKey extends keyof TLocales[keyof TLocales] & string>(
    key: TKey,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ...variables: any[]
  ): string
}
