/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Given a set of `variables` arguments, interpolates said arguments into a
 * returned string.
 *
 * @param variables Variadic set of variables used for message interpolation
 * @typeparam `TVariables` Type of the required interpolation variables
 * @typeparam `TMessage` Type of message returned, provided for string narrowing
 * @returns Translated message with variables interpolated if appropriate
 */
export type MessageInterpolator<
  TVariables extends any[] = any[],
  TMessage extends string = string
> = (...variables: TVariables) => TMessage
