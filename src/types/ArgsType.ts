/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Returns the variadic argument types of the provided `TFunction`
 *
 * @typeparam `TFunction` Function type that accepts any number of arguments
 *
 * @example
 *
 * // Fictitious function type that accepts a string array and number
 * type Aggregator = (accumulation: string[], element: number) => string
 *
 * // Extract the argument types from the fictitious function type
 * type Args = ArgsType<Aggregator>
 *
 * // => [string[], number]
 */
export type ArgsType<
  TFunction extends (...args: any) => any
> = TFunction extends (...args: infer TArgs) => any ? TArgs : unknown[]
