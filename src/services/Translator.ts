import { ArgsType } from '../types/ArgsType'
import { ILocaleConfigurationProvider } from '../types/ILocaleConfigurationProvider'
import { ILocaleMessageProvider } from '../types/ILocaleMessageProvider'
import { ITranslator } from '../types/ITranslator'
import { LocaleMap } from '../types/LocaleMap'
import { MessageInterpolator } from '../types/MessageInterpolator'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export class Translator<TLocales extends LocaleMap = any>
  implements ITranslator<TLocales> {
  constructor(
    private readonly localeMessages: ILocaleMessageProvider<TLocales>,
    private readonly localeConfig: ILocaleConfigurationProvider<
      keyof TLocales & string
    >
  ) {}

  /**
   * Finds a message entry associated with the provided `key` and returns a
   * translated message given the chosen locale.  This may also require
   * addition arguments depending on the entry to interpolate values into the
   * resulting message.
   *
   * @param key Key matching the desired translated message to use
   * @param args Any arguments the entry may required to be interpolated into
   *   its message
   * @typeparam `TKey` Type of the entry key requested.  This type is used to
   *   infer the required interpolation arguments for the desired entry.
   */
  public translate<TKey extends keyof TLocales[keyof TLocales] & string>(
    key: TKey,
    ...args: ArgsType<TLocales[keyof TLocales][TKey]>
  ) {
    const interpolate: MessageInterpolator | undefined = this.findInterpolator(
      key
    )

    if (interpolate === undefined) {
      return key
    }

    try {
      // Attempt to return the appropriate message with variables interpolated
      return interpolate(...args)
    } catch (_ /* Unused interpolation error */) {
      // If we get any errors, safely return the translation key
      return key
    }
  }

  private get defaultLocale() {
    return this.localeMessages.messages[this.localeConfig.defaultLocale]
  }

  private get locale() {
    return this.localeMessages.messages[this.localeConfig.currentLocale]
  }

  /**
   * Finds a message interpolation function matching `key` for the current
   * locale or default locale.
   *
   * @param key Key matching the desired translated message interpolator
   * @returns a message interpolation function from the current or default
   *   locale or `undefined` if none exists
   */
  private findInterpolator<
    TKey extends keyof TLocales[keyof TLocales] & string
  >(key: TKey) {
    /**
     * If the locale exists and the requested interpolator matching `key` is a
     * proper function, return it
     */
    if (this.locale !== undefined && typeof this.locale[key] === 'function') {
      return this.locale[key]
    }

    /**
     * Since the requested interpolator could not be found with the current
     * locale, return the matching interpolator in the default locale if such a
     * proper function exists
     */
    if (
      this.defaultLocale !== undefined &&
      typeof this.defaultLocale[key] === 'function'
    ) {
      return this.defaultLocale[key]
    }

    // Otherwise, return `undefined`
    return undefined
  }
}
