import { ILocaleConfigurationProvider } from '../types/ILocaleConfigurationProvider'
import { ILocaleMessageProvider } from '../types/ILocaleMessageProvider'
import { LocaleMap } from '../types/LocaleMap'
import { Translator } from './Translator'

const messages = {
  en: {
    english: () => 'English',
    error: () => {
      throw new Error()
    },
    help: () => 'Help me!',
    introduction: (name: string) => `My name is ${name}`
  },
  fr: {
    error: () => {
      throw new Error()
    },
    french: () => 'Francais',
    help: () => 'Aide-moi!',
    introduction: (name: string) => `Je m'appel ${name}`
  }
}
type Messages = typeof messages
type Keys = keyof (Messages[keyof Messages])

describe(Translator, () => {
  describe('#translate', () => {
    let translator: Translator<Messages>

    beforeEach(() => {
      translator = new Translator(
        {
          messages
        },
        {
          currentLocale: 'fr',
          defaultLocale: 'en'
        }
      )
    })

    it('returns a translated message when the given key is found for the chosen locale', () => {
      expect(translator.translate('help')).toEqual('Aide-moi!')
    })

    it('returns a translated message from the fallback locale when no key is found for the chosen locale', () => {
      expect(translator.translate('english' as Keys)).toEqual('English')
    })

    it('returns the given key if no message is found in the chosen locale nor in the default locale', () => {
      expect(translator.translate('random12' as Keys)).toEqual('random12')
    })

    it(
      'returns the given key if a message was found but throws an exception ' +
        'when attempting to interpolate the provided variables',
      () => {
        expect(translator.translate('error')).toEqual('error')
      }
    )
  })

  describe('#translate with classes', () => {
    class Provider<TLocales extends LocaleMap>
      implements ILocaleMessageProvider<TLocales> {
      constructor(public readonly messages: TLocales) {}
    }

    class Config<TLocale extends string>
      implements ILocaleConfigurationProvider<TLocale> {
      constructor(
        public locales: TLocale[],
        public defaultLocale: TLocale,
        public currentLocale: TLocale = defaultLocale
      ) {}
    }

    const config = new Config(['en', 'fr'], 'en', 'fr')
    const provider = new Provider(messages)
    let translator: Translator<Messages>

    beforeEach(() => {
      translator = new Translator(provider, config)
    })

    it('returns a translated message when the given key is found for the chosen locale', () => {
      config.currentLocale = 'fr'

      expect(translator.translate('help')).toEqual('Aide-moi!')
    })

    it('returns a translated message from the fallback locale when no key is found for the chosen locale', () => {
      config.currentLocale = 'fr'
      config.defaultLocale = 'en'

      /**
       * Key must be asserted as otherwise returns a type error since it does
       * not exist for all locales
       */
      expect(translator.translate('english' as Keys)).toEqual('English')
    })

    it('returns the given key if no message is found in the chosen locale nor in the default locale', () => {
      /**
       * Key must be asserted as otherwise returns a type error since it does
       * not exist for any locale
       */
      expect(translator.translate('random12' as Keys)).toEqual('random12')
    })

    it(
      'returns the given key if a message was found but throws an exception ' +
        'when attempting to interpolate the provided variables',
      () => {
        expect(translator.translate('error')).toEqual('error')
      }
    )
  })
})
