import { ILocaleConfigurationProvider } from '../types/ILocaleConfigurationProvider'

export class LocaleConfigurationProvider<TLocale extends string = string>
  implements ILocaleConfigurationProvider<TLocale> {
  constructor(
    public locales: TLocale[],
    public defaultLocale: TLocale,
    public currentLocale: TLocale = defaultLocale
  ) {}
}
