import { LocaleConfigurationProvider } from './LocaleConfigurationProvider'

describe(LocaleConfigurationProvider, () => {
  let config: LocaleConfigurationProvider

  beforeEach(() => {
    config = new LocaleConfigurationProvider(['en', 'fr'], 'en')
  })

  it('exposes the `currentLocale` provided to the constructor', () => {
    expect(config.currentLocale).toEqual('en')
  })

  it('exposes the `defaultLocale` provided to the constructor', () => {
    expect(config.defaultLocale).toEqual('en')
  })

  it('exposes the `locales` provided to the constructor', () => {
    expect(config.locales).toEqual(['en', 'fr'])
  })
})
