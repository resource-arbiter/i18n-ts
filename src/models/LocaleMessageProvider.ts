import { ILocaleMessageProvider } from '../types/ILocaleMessageProvider'
import { LocaleMap } from '../types/LocaleMap'

export class LocaleMessageProvider<TLocales extends LocaleMap = LocaleMap>
  implements ILocaleMessageProvider<TLocales> {
  constructor(public readonly messages: TLocales) {}
}
