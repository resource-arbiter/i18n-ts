import { LocaleMap } from '../types/LocaleMap'
import { LocaleMessageProvider } from './LocaleMessageProvider'

describe(LocaleMessageProvider, () => {
  let messages: LocaleMap
  let provider: LocaleMessageProvider

  beforeEach(() => {
    messages = {
      en: {
        names: () => `Hey there`
      }
    }

    provider = new LocaleMessageProvider(messages)
  })

  it('exposes the `messages` provided to the constructor', () => {
    expect(provider.messages).toMatchObject(messages)
  })
})
