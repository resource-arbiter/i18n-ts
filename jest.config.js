module.exports = {
  collectCoverageFrom: ['src/**/*', '!src/index.ts'],
  preset: 'ts-jest',
  reporters: ['default', ['jest-junit', { outputDirectory: './coverage' }]],
  testEnvironment: 'node'
}
