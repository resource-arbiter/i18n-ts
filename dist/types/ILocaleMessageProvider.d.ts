import { LocaleMap } from './LocaleMap';
/**
 * Data model holding translated messages as string interpolation functions
 *
 * @example
 *
 * class Provider<TLocales extends LocaleMap>
 *   implements ILocaleMessageProvider<TLocales> {
 *   constructor(public readonly messages: TLocales) {}
 * }
 */
export interface ILocaleMessageProvider<TLocaleMessageMap extends LocaleMap = LocaleMap> {
    messages: TLocaleMessageMap;
}
