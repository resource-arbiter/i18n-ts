import { MessageInterpolator } from './MessageInterpolator';
/**
 * Object map of message interpolator functions.  Functions are expected to
 * return a translated message.
 *
 * @typeparam `TKeys` Type of message keys, provided for string narrowing
 * @typeparam `TMessageInterpolater` Type of message interpolation function,
 *   provided to narrow the interpolation function a one accepting specific
 *   arguments
 */
export declare type MessageMap<TKeys extends string = string, TMessageInterpolator extends MessageInterpolator = MessageInterpolator> = Record<TKeys, TMessageInterpolator>;
