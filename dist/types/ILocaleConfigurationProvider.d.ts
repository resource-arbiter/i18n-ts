/**
 * Data model exposing key locale attributes (_current_ and _default_)
 *
 * @example
 *
 * class Config<TLocale extends string>
 *   implements ILocaleConfigurationProvider<TLocale> {
 *   constructor(
 *     public locales: TLocale[],
 *     public defaultLocale: TLocale,
 *     public currentLocale: TLocale = defaultLocale
 *   ) {}
 * }
 */
export interface ILocaleConfigurationProvider<TLocale extends string = string> {
    currentLocale: TLocale;
    defaultLocale: TLocale;
}
