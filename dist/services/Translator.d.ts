import { ArgsType } from '../types/ArgsType';
import { ILocaleConfigurationProvider } from '../types/ILocaleConfigurationProvider';
import { ILocaleMessageProvider } from '../types/ILocaleMessageProvider';
import { ITranslator } from '../types/ITranslator';
import { LocaleMap } from '../types/LocaleMap';
export declare class Translator<TLocales extends LocaleMap = any> implements ITranslator<TLocales> {
    private readonly localeMessages;
    private readonly localeConfig;
    constructor(localeMessages: ILocaleMessageProvider<TLocales>, localeConfig: ILocaleConfigurationProvider<keyof TLocales & string>);
    /**
     * Finds a message entry associated with the provided `key` and returns a
     * translated message given the chosen locale.  This may also require
     * addition arguments depending on the entry to interpolate values into the
     * resulting message.
     *
     * @param key Key matching the desired translated message to use
     * @param args Any arguments the entry may required to be interpolated into
     *   its message
     * @typeparam `TKey` Type of the entry key requested.  This type is used to
     *   infer the required interpolation arguments for the desired entry.
     */
    translate<TKey extends keyof TLocales[keyof TLocales] & string>(key: TKey, ...args: ArgsType<TLocales[keyof TLocales][TKey]>): string;
    private readonly defaultLocale;
    private readonly locale;
    /**
     * Finds a message interpolation function matching `key` for the current
     * locale or default locale.
     *
     * @param key Key matching the desired translated message interpolator
     * @returns a message interpolation function from the current or default
     *   locale or `undefined` if none exists
     */
    private findInterpolator;
}
