import { ILocaleMessageProvider } from '../types/ILocaleMessageProvider';
import { LocaleMap } from '../types/LocaleMap';
export declare class LocaleMessageProvider<TLocales extends LocaleMap = LocaleMap> implements ILocaleMessageProvider<TLocales> {
    readonly messages: TLocales;
    constructor(messages: TLocales);
}
