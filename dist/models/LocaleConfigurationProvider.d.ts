import { ILocaleConfigurationProvider } from '../types/ILocaleConfigurationProvider';
export declare class LocaleConfigurationProvider<TLocale extends string = string> implements ILocaleConfigurationProvider<TLocale> {
    locales: TLocale[];
    defaultLocale: TLocale;
    currentLocale: TLocale;
    constructor(locales: TLocale[], defaultLocale: TLocale, currentLocale?: TLocale);
}
